class Component {
  constructor({minValue, maxValue, unit="something", name, help="tbd", startValue=0, interval=1000}) {
    let getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

    this.name = name
    this.help = help

    this.min = minValue
    this.max = maxValue
    this.unit = unit
    this.value = startValue
    this.B = Math.PI / 2
    this.unitsTranslatedToTheRight = getRandomInt(0, 5)
  }
  amplitude() {
    return (this.max-this.min)/2
  }
  unitsTranslatedUp() {
    return this.min + this.amplitude()
  }
  getLevel(t) {
    let level = this.amplitude() * Math.cos(this.B *(t-this.unitsTranslatedToTheRight)) + this.unitsTranslatedUp()
    return level
  }

  start() {
    return setInterval(() => {
      let now = new Date()
      let t = now.getMinutes() + now.getSeconds() / 100
      let componentValue = this.getLevel(t)
      // current value
      this.value = componentValue
      //console.log("Ⓜ️ metric value[weather_temperature]", componentValue)

    }, this.interval)  

  }
}

module.exports = { Component: Component }